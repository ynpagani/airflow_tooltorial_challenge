import sqlite3
from pathlib import Path
import pandas as pd


SELECT_ORDERS = """
    SELECT *
    FROM 'Order';
"""

SELECT_ORDER_DETAILS = """
    SELECT *
    FROM OrderDetail;
"""


def extract_order_table():
    """
    Extracts the Order table from the database and places it in a csv file
    """

    # Context manager to create and handle the connection to the sqlite db
    with sqlite3.connect(Path("data/Northwind_small.sqlite")) as conn:
        # Creates a dataframe with the output of the query
        df = pd.read_sql_query(SELECT_ORDERS, conn)

        # Create folder to dump data from tasks
        solution_folder = Path("challenge_data/")
        solution_folder.mkdir(parents=True, exist_ok=True)

        df.to_csv(solution_folder / "output_orders.csv", sep=';', index=False)


def extract_and_process_order_detail():
    """
    Extracts Order Detail table from sqlite db and then JOINs it with Order table to infer quantity sold sum to 
    Rio de Janeiro.
    """
    
    # Context manager to create and handle the connection to the sqlite db
    with sqlite3.connect(Path("data/Northwind_small.sqlite")) as conn:
        # Creates a dataframe for OrderDetail table from the query
        order_detail_df = pd.read_sql_query(SELECT_ORDER_DETAILS, conn)
        
        # Creates a dataframe for Order table from the .csv file
        order_df = pd.read_csv(Path("challenge_data/output_orders.csv"), sep=';')

        # Performs a INNER JOIN on OrderDetails.OrderId == Order.Id
        join_df = order_detail_df.join(order_df.set_index("Id"), on="OrderId", how="inner")

        # Selects only the rows with ShipCity == "Rio de Janeiro"
        city_df = join_df[join_df["ShipCity"] == "Rio de Janeiro"]
        
        # Performs sum of Quantity column values
        quantity_count = city_df["Quantity"].sum()

        # Saves the answer to a text file
        with open(Path("count.txt"), "w") as f:
            f.write(str(quantity_count))



if __name__ == "__main__":
    pass