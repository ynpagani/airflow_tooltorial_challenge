from datetime import datetime, timedelta
from airflow.operators.python import PythonOperator
from airflow import DAG
from airflow.models import Variable
from task_functions import extract_order_table, extract_and_process_order_detail

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}


## Do not change the code below this line ---------------------!!#
def export_final_answer():
    import base64

    # Import count
    with open('count.txt') as f:
        count = f.readlines()[0]

    my_email = Variable.get("my_email")
    message = my_email+count
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')

    with open("final_output.txt","w") as f:
        f.write(base64_message)
    return None

## Do not change the code above this line-----------------------##

with DAG(
    'DesafioAirflow',
    default_args=default_args,
    description='Desafio de Airflow da Indicium',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['example'],
) as dag:
    dag.doc_md = """
        Esse é o desafio de Airflow da Indicium.
    """
   
    extract_order = PythonOperator(
        task_id='extract_order_table',
        python_callable=extract_order_table,
        provide_context=True
    )
    extract_order.doc_md = """
        Tarefa para gerar um .csv com o conteúdo da tabela Order.
    """
    
    process_order_and_order_detail = PythonOperator(
        task_id='process_order_and_order_detail',
        python_callable=extract_and_process_order_detail,
        provide_context=True
    )
    process_order_and_order_detail.doc_md = """
        Extrai tabela OrderDetail e a processa junto com a tabela Order. Por fim, infere quantidade vendida para uma
        cidade específica e salva o resultado em count.txt.
    """


    export_final_output = PythonOperator(
        task_id='export_final_output',
        python_callable=export_final_answer,
        provide_context=True
    )

    # Tasks relationship
    extract_order >> process_order_and_order_detail >> export_final_output