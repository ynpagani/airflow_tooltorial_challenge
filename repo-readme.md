## O Airflow

do site do próprio airflow:

    Airflow is a platform created by the community to programmatically author, schedule and monitor workflows.


## Primeiras Impressões

Para ter uma ideia do que se trata a ferramenta, primeiro vamos executar o Airflow localmente com os DAGS(Directed acyclic graphs, o conceito usado para implementar pipelines).

para instalar o airflow vamos primeiro criar um virtualenv e depois rodar o script install.sh. Esse script é um ctrl c ctrl v das instruções encontradas no site.

```
virtualenv venv -p python3
source venv/bin/activate
pip install -r requirements.txt
bash install.sh
```

Se as coisas deram certo, no terminal vai aparecer a seguinte mensagem:

```
standalone | 
standalone | Airflow is ready
standalone | Login with username: admin  password: sWFbFYrnYFAfYgY3
standalone | Airflow Standalone is for development purposes only. Do not use this in production!
standalone |
```

airflow roda na porta 8080, então podemos acessar em 
http://localhost:8080

tomar um tempo aqui para ver a interface, as dags, tome um tempo para explorar a interface.

## Limpando os Dags de Exemplo

Para tirar os dags de exemplo e começar um dag nosso, primeiramente apertamos **CTRL+C** no terminal onde esta rodando o airflow a fim de interrompe-lo, então podemos editar o arquivo airflow.cfg trocando:
```
load_examples = True
```
para
```
load_examples = False
```

Feito isso, primeiro precisamos configurar o ambiente para dizer onde vão ficar os arquivos de config do airflow, fazemos isso configurando a seguinte variavel de ambiente:

```
export AIRFLOW_HOME=./airflow-data
```

Dessa forma configuramos o airflow para colocar suas configurações dentro da pasta desse tutorial na pasta /airflow-data

Na sequência rodamos o comando para resetar o db do airflow e fazer start do airflow local:

```
airflow db reset
airflow standalone
```
