# Solução do Desafio Airflow
Para que a solução execute de forma correta, é necessário fazer a configuração inicial do repositório. Para isso, siga os passos descritos em [setup do repositório e airflow](repo-readme.md).  
  

Feito o setup, agora é preciso criar uma pasta chamada **dags** dentro da pasta **airflow-data**. Em seguida, mova os arquivos  *challenge_dag.py* e *task_functions.py* para dentro dessa nova pasta **airflow-data/dags/**.  
  
    Os arquivos python challenge_dag.py e task_functions.py contém implementação da solução do desafio, logo é importante que eles estejam no lugar certo.
  
Se os passos anteriores foram executados de forma correta, você deve ter uma estrutura de repositório similar a imagem abaixo:
  
  ![Estrutura Esperada para o Repositório](repo_structure.png)

Agora é possível verificar no Airflow a DAG da solução do desafio:
  
  ![DAG solução](dag_solution.png)

  *Caso a DAG esteja demorando para aparecer na lista, ou então, ocorreu algum erro de importe. Finalize o Airflow com **CTRL+C** e rode novamente o Airflow com `airflow standalone` no terminal.*

Adicione uma variável no Airflow com a `key` "my_email" e no campo "value" adicione seu email @indicium.tech. Exemplo:

![](variable.png)

### Por fim, execute a DAG e verifique a criação dos arquivos **count.txt** e **final_output.txt** que contém informações sobre o resultado final.